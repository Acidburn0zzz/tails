# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-04-29 15:20+0000\n"
"PO-Revision-Date: 2015-10-26 16:36+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/test_10-"
"rc1/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Call for testing: 1.0~rc1\"]]\n"
msgstr "[[!meta title=\"درخواست برای آزمایش: 1.0~rc1\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"2014-04-18 12:00:00\"]]\n"
msgstr "[[!meta date=\"۲۰۱۴/۰۴/۱۸ ۱۲:۰۰:۰۰\"]]\n"

#. type: Plain text
msgid ""
"You can help Tails! The first release candidate for the upcoming version 1.0 "
"is out. Please test it and see if it works for you."
msgstr ""
"شما می‌توانید به تیلز کمک کنید! اولین نامزد انتشار برای نسخهٔ پیش‌روی ۱٫۰ آماده "
"شده‌است. لطفاً آن را امتحان کنید و ببینید برایتان کار می‌کند یا نه."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"How to test Tails 1.0~rc1?\n"
"=============================\n"
msgstr ""
"چگونه تیلز 1.0~rc1 را امتحان کنیم؟\n"
"=============================\n"

#. type: Bullet: '1. '
msgid ""
"**Keep in mind that this is a test image.** We have made sure that it is not "
"broken in an obvious way, but it might still contain undiscovered issues."
msgstr ""
"**در خاطر داشته باشید که این یک تصویر آزمایشی است.** مطمئن شده‌ایم که هیچ "
"آسیب مشهودی ندارد، اما همچنان ممکن است مشکلاتی شناسایی‌نشده داشته باشد."

#. type: Bullet: '2. '
msgid ""
"Either try the <a href=\"#automatic_upgrade\">automatic upgrade</a>, or "
"download the ISO image and its signature:"
msgstr ""
"یا از <a href=\"#automatic_upgrade\">ارتقای خودکار</a> استفاده کرده یا تصویر "
"ایزو و امضای آن را دانلود کنید:"

#. type: Plain text
#, no-wrap
msgid "   <a class=\"download-file\" href=\"http://dl.amnesia.boum.org/tails/alpha/tails-i386-1.0~rc1/tails-i386-1.0~rc1.iso\" >Tails 1.0~rc1 ISO image</a>\n"
msgstr "   <a class=\"download-file\" href=\"http://dl.amnesia.boum.org/tails/alpha/tails-i386-1.0~rc1/tails-i386-1.0~rc1.iso\" >تصویر ایزوی تیلز 1.0~rc1</a>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <a class=\"download-signature\"\n"
"   href=\"https://tails.boum.org/torrents/files/tails-i386-1.0~rc1.iso.sig\">Tails 1.0~rc1 signature</a>\n"
msgstr ""
"   <a class=\"download-signature\"\n"
"   href=\"https://tails.boum.org/torrents/files/tails-i386-1.0~rc1.iso.sig\">امضای تیلز 1.0~rc1</a>\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid "[[Verify the ISO image|doc/get/verify]]."
msgid "Verify the ISO image."
msgstr "[[تأیید تصویر ایزو|doc/get/verify]]."

#. type: Bullet: '1. '
msgid ""
"Have a look at the list of [[longstanding known issues|support/"
"known_issues]]."
msgstr ""
"نگاهی به فهرست [[مشکلات شناسایی‌شدهٔ قدیمی|support/known_issues]] بیندازید."

#. type: Bullet: '1. '
msgid "Test wildly!"
msgstr "به شدت آن را امتحان کنید!"

#. type: Plain text
msgid ""
"If you find anything that is not working as it should, please [[report to us|"
"doc/first_steps/bug_reporting]]! Bonus points if you check if it is a "
"[[longstanding known issue|support/known_issues]]."
msgstr ""
"اگر هر چیزی یافتید که مطابق انتظار کار نمی‌کند لطفاً [[به ما گزارش بدهید|doc/"
"first_steps/bug_reporting]]! چه بهتر که مطمئن شده باشید این مشکل یک [[مشکل "
"شناسایی‌شدهٔ قدیمی|support/known_issues]] نیست."

#. type: Plain text
#, no-wrap
msgid "<div id=\"automatic_upgrade\"></a>\n"
msgstr "<div id=\"automatic_upgrade\"></a>\n"

#. type: Title =
#, no-wrap
msgid "How to automatically upgrade from 0.23?\n"
msgstr "چگونه از ۰٫۲۳ ارتقای خودکار را انجام بدهم؟\n"

#. type: Plain text
#, no-wrap
msgid ""
"These steps allow you to automatically upgrade a device installed with <span\n"
"class=\"application\">Tails Installer</span> from Tails 0.23 to Tails 1.0~rc1.\n"
msgstr ""
"این مراحل به شما اجازه می‌دهند تا به طور خودکار یک دستگاه نصب‌شده با <span\n"
"class=\"application\">Tails Installer</span> از تیلز ۰٫۲۳ به تیلز 1.0~rc1 ارتقاء دهید.\n"

#. type: Bullet: '1. '
msgid ""
"Start Tails 0.23 and [[set an administration password|doc/first_steps/"
"startup_options/administration_password]]."
msgstr ""
"تیلز ۰٫۲۳ را راه‌اندازی کرده و[[یک گذرواژهٔ مدیریتی ایجاد کنید|doc/first_steps/"
"startup_options/administration_password]]."

#. type: Bullet: '1. '
msgid ""
"Run this command in a <span class=\"application\">Root Terminal</span> to "
"select the \"alpha\" upgrade channel and start the upgrade:"
msgstr ""
"این فرمان را در یک <span class=\"application\">پایانهٔ اصلی</span> اجرا کرده "
"تا یک کانال ارتقای «آلفا» پیدا کنید و ارتقاء را آغاز کنید:"

#. type: Plain text
#, no-wrap
msgid ""
"       echo TAILS_CHANNEL=\\\"alpha\\\" >> /etc/os-release && \\\n"
"            tails-upgrade-frontend-wrapper\n"
msgstr ""
"       echo TAILS_CHANNEL=\\\"alpha\\\" >> /etc/os-release && \\\n"
"            tails-upgrade-frontend-wrapper\n"

#. type: Plain text
#, no-wrap
msgid ""
"1. Once the upgrade has been installed, restart Tails and choose\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">System</span>&nbsp;&#x25B8;\n"
"     <span class=\"guimenuitem\">About Tails</span>\n"
"   </span>\n"
"   to confirm that the running system is Tails 1.0~rc1.\n"
msgstr ""
"۱. پس از نصب شدن ارتقاءدهنده تیلز را دوباره راه‌اندازی کرده و از\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">سیستم</span>&nbsp;&#x25B8;\n"
"     <span class=\"guimenuitem\">دربارهٔ تیلز</span>\n"
"   </span>\n"
"   بروید تا تأیید کنید که این سیستم تیلز 1.0~rc1 است.\n"

#. type: Title =
#, no-wrap
msgid "What's new since 0.23?\n"
msgstr "چه چیزهایی نسبت به ۰٫۲۳ جدید هستند؟\n"

#. type: Plain text
#, no-wrap
msgid ""
"* Bugfixes\n"
"  - Disable inbound I2P connections. Tails already restricts incoming\n"
"    connections, but this change tells I2P about it.\n"
"  - Fix link to the system requirements documentation page in the Tails\n"
"    Upgrader error shown when too little RAM is available.\n"
msgstr ""
"* رفع ایرادها\n"
"  - غیرفعال کردن اتصال‌های رو به داخل I2P. تیلز اتصال‌های\n"
"    رو به داخل را قطع می‌کند اما این مورد خاص برای I2P است.\n"
"  - درست کردن پیوند به صفحهٔ سند نیازمندی‌های سیستم در \n"
"    پیغام خطای ارتقاءدهندّد تیلز زمانی که حافظهٔ تصادفی کمی موجود است.\n"

#. type: Plain text
#, no-wrap
msgid ""
"* Minor improvements\n"
"  - Upgrade I2P to 0.9.12-2~deb6u+1.\n"
"  - Import TorBrowser profile. This was forgotten in Tails 0.23 and even\n"
"    though we didn't explicitly set those preferences in that release\n"
"    they defaulted to the same values. This future-proofs us in case the\n"
"    defaults would ever change.\n"
"  - Import new custom version of Tor Launcher:\n"
"    * Based on upstream Tor Launcher 0.2.5.3.\n"
"    * Improve how Tor Launcher handles incomplete translation.  (Tor\n"
"      bug [[!tor_bug 11483]]; more future-proof fix for Tails bug\n"
"      [[!tails_ticket 6885]])\n"
"    * Remove the bridge settings prompt. (Tor bug [[!tor_bug 11482]];\n"
"      closes Tails bug [[!tails_ticket 6934]])\n"
"    * Always show bridge help button. (Tor bug [[!tor_bug 11484]])\n"
msgstr ""
"* بهبودهای جزیی:\n"
"  - ارتقای I2P به 0.9.12-2~deb6u+1.\n"
"  - وارد کردن پروفایل مرورگر تور. این کار در تیلز ۰٬۲۳ فراموش شده بود و\n"
"    با این که به طرز آشکار این ترجیحات را در آن انتشار مشخص نکرده بودیم\n"
"    آن‌ها به طور پیش‌فرض روی همان مقدار قرار گرفتند. این مساله برای آینده \n"
"    در صورتی که مقدار پیش‌فرض عوض نشد مناسب است.\n"
"  - وارد کردن نسخهٔ جدید اختصاصی راه‌اندار تور:\n"
"    * بر مبنای آپ‌استریم راه‌انداز تور ۰٫۲٫۵٫۳.\n"
"    * بهبود نحوهٔ رسیدگی ترجمه‌های ناتمام از سوی راه‌انداز تور (ایراد\n"
"      تور [[!tor_bug 11483]]؛ موارد مهم برای آینده برای ایراد تیلز\n"
"      [[!tails_ticket 6885]])\n"
"    * برداشتن تنظیمات پل. (ایراد تور [[!tor_bug 11482]]؛\n"
"      باعث رفع این ایراد می‌شود [[!tails_ticket 6934]])\n"
"    * همیشه دکمهٔ کمک پل نشان داده شود (ایراد تور [[!tor_bug 11484]])\n"

#. type: Plain text
#, no-wrap
msgid ""
"See the <a href=\"https://git-tails.immerda.ch/tails/plain/debian/changelog?h=stable\">online\n"
"Changelog</a> for technical details.\n"
msgstr ""
"برای جزییات فنی رجوع کنید به <a href=\"https://git-tails.immerda.ch/tails/plain/debian/changelog?h=stable\">\n"
"لاگ آنلاین تغییرات</a>.\n"
